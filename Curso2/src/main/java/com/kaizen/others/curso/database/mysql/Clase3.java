package com.kaizen.others.curso.database.mysql;

import java.nio.channels.ClosedSelectorException;
import java.util.LinkedList;

public class Clase3
{
    private static int contador;
    private static LinkedList<Clase3> lista;

    protected int id;

    static
    {
        contador = 0;
        lista = new LinkedList<>();
    }

    public Clase3()
    {
        contador++;
        id = contador;
        lista.add(this);
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public static int getContador()
    {
        return contador;
    }

    protected void addNumber(int numero)
    {
        int numero2 = 10;
        id += numero;
    }

    static LinkedList<Clase3> getLista()
    {
        return lista;
    }
}
